var base_url = 'http://biocarewebpanel.ap-south-1.elasticbeanstalk.com/api'
const urlObj = {
  login_url: base_url + '/user/auth/signin',
  register_url: base_url + '/user/auth/signup',
  family_member_url: base_url + '/user/members/list',
  banner_url: base_url + '/user/home/banner',
  getProfile_url: base_url + '/user/profile/view',
  my_appointment_url: base_url + '/user/doctorAppointment/view',
  update_profile_url: base_url + '/user/profile/update',
  find_doctor_url: base_url + '/doctor/auth/search-doctors',
  find_pharmacy_url  : base_url + '/user/labtest/findAllLabTest',
  find_lab_url: base_url + '/user/auth/search-labs',
  doctorcat_url: base_url + '/doctor/auth/category-list',
  shared_video_url: base_url + '/user/videos/view/',

};
module.exports = urlObj;

import axios from 'axios';
export const findpharmacy = (state, token) => {
  return async (dispatch, getState) => {
    var url = urlObj.find_pharmacy_url + '?city=' + state.city + '&patient_id='+state.patient_id
    var headers1 = {
      headers: {
        'Content-Type': `application/json`,
        'Authorization': `Bearer ${token}`
      }
    }
    console.log(headers1, url)
    await axios
      .get(url, headers1)
      .then((response) => {
        dispatch({
          type: 'FINDPHARMACY',
          payload: response.data,

        });
        // console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const pharmacy_category = () => {
  return async (dispatch, getState) => {
    var url = urlObj.doctorcat_url;

    await axios
      .get(url)
      .then((response) => {
        dispatch({
          type: 'FINDPHARMACYLIST',
          payload: response.data,

        });
        // console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
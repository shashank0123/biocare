import axios from 'axios';
import urlObj from '../../url';
export const finddoctors = (state, token) => {
  return async (dispatch, getState) => {
    var url = urlObj.find_doctor_url + '?city=' + state.city + '&category_id=' + state.doctor_category
    var headers1 = {
      headers: {
        'Content-Type': `application/json`,
        'Authorization': `Bearer ${token}`
      }
    }
    console.log(headers1, url)
    await axios
      .get(url, headers1)
      .then((response) => {
        dispatch({
          type: 'FINDDOCTORS',
          payload: response.data,

        });
        // console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const doc_category = () => {
  return async (dispatch, getState) => {
    var url = urlObj.doctorcat_url;

    await axios
      .get(url)
      .then((response) => {
        dispatch({
          type: 'DOCCATLIST',
          payload: response.data,

        });
        // console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
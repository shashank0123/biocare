import axios from 'axios';
import urlObj from '../../url';
import AsyncStorage from '@react-native-community/async-storage';
export const registeration = (state) => {
  return async (dispatch, getState) => {

    var url = urlObj.register_url;
    var headers1 = {
      headers: {
        'Content-Type': `application/json`
      }
    }
    var data = { name: state.name,password:state.password,city:state.city,phone:state.phone }
    console.log(url, data, headers1)
    await axios
    .post(url, data, headers1)
      .then(async (response) => {
        console.log(response.data ,'check this...')
      

        dispatch({
          type: 'REGISTER',
          payload: response.data

        });
        // console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
import axios from 'axios';
import urlObj from '../../url';
import AsyncStorage from '@react-native-community/async-storage';
export const saveprofile = (token, state) => {
  return async (dispatch, getState) => {
    var headers1 = {
      headers: {
        'Content-Type': `application/json`,
        'Authorization': `Bearer ${token}`
      }
    }
    var url = urlObj.update_profile_url;
    var data = {
      id: state.patientId,
      userId: state.userId,
      avatarUrl: state.profile_image,
      phone: state.phone,
      name: state.name,
      password: state.password,
      dob: state.dob,
      city: state.city,
      bloodGroup: state.bloodGroup,
      gender: state.gender,
      email: state.email,
      adress: state.adress,
      state: state.state,
      pincode: state.pincode,
      country: state.country
    }
    console.log(url, data, headers1, 'Sending these..............')
    await axios
      .post(url, data, headers1)
      .then(async (response) => {
        console.log(response.data.data)
        dispatch({
          type: 'SAVEPROFILE',
          payload: response.data.data
        });
        // console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
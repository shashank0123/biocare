import axios from 'axios';
import urlObj from '../../url';
export const videoList = (id, token) => {
  return async (dispatch, getState) => {
    var url = urlObj.shared_video_url + id;
    var headers1 = {
      headers: {
        'Content-Type': `application/json`,
        'Authorization': `Bearer ${token}`
      }
    }
    console.log(headers1, url)
    await axios
      .get(url, headers1)
      .then((response) => {
        dispatch({
          type: 'VIDEOLIST',
          payload: response.data

        });
        // console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };
};

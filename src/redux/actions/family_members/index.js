import axios from 'axios';
import urlObj from '../../url';
import AsyncStorage from '@react-native-community/async-storage';
export const family_members = (token) => {
  return async (dispatch, getState) => {
    var url = urlObj.family_member_url;
    console.log(url, token)
    var headers1 = {
      headers: {
        'Content-Type': `application/json`,
        'Authorization': `Bearer ${token}`
      }
    }

    console.log(url, headers1)

    await axios
      .get(url, headers1)
      .then((response) => {
        console.log(response.data)
        dispatch({
          type: 'FAMILY_MEMBERS',
          payload: response.data

        });
      })
      .catch((error) => {
        console.log(error);
      });
  };
};



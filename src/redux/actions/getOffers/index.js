import axios from 'axios';
import urlObj from '../../url';
import AsyncStorage from '@react-native-community/async-storage';
export const getOffers = (token) => {
  return async (dispatch, getState) => {
    var url = urlObj.banner_url;
    var headers1 = {
      headers: {
        'Content-Type': `application/json`,
        'Authorization': `Bearer ${token}`
      }
    }

   console.log(url,headers1)

    await axios
    .get(url, headers1)
    .then((response) => {
        console.log(response.data)
      dispatch({
        type: 'GETOFFERS',
        payload: response.data

      });
    })
    .catch((error) => {
      console.log(error);
    });
};
};



import axios from 'axios';
import urlObj from '../../url';
import AsyncStorage from '@react-native-community/async-storage';
export const getProfile = (token,patientId) => {
  return async (dispatch, getState) => {
    var url = urlObj.getProfile_url + "/" + patientId ;
    var headers1 = {
      headers: {
        'Content-Type': `application/json`,
        'Authorization': `Bearer ${token}`
      }
    }
    // var data = {patientId:patientId }
   console.log(url,headers1)

    await axios
    .get(url, headers1)
    .then((response) => {
        console.log(response.data)
      dispatch({
        type: 'GETPROFILE',
        payload: response.data

      });
    })
    .catch((error) => {
      // console.log(error);
    });
};
};



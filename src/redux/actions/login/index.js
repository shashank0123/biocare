import axios from 'axios';
import urlObj from '../../url';
import AsyncStorage from '@react-native-community/async-storage';
export const login = state => {
  return async (dispatch, getState) => {
    var url = urlObj.login_url;
    var headers1 = {
      headers: {
        'Content-Type': `application/json`,
      },
    };
    var data = { username: state.phone, password: state.password };
    console.log(url, data, headers1);
    await axios
      .post(url, data, headers1)
      .then(async response => {
        console.log(response.data);

        if (response.data.token) {
          await AsyncStorage.setItem('token', response.data.token);

          dispatch({
            type: 'LOGIN',
            payload: response.data.data,
          });
        }

        // console.log(response);
      })
      .catch(error => {
        if (error.response.status === 401) {
          // Handle 400
          console.log(error.response, 'here is error');
          dispatch({
            type: 'LOGIN',
            payload: error.response.data,
          });
        } else {
          // Handle else
        }
        console.log(error);
      });
  };
};

export const checklogin = () => {
  return async (dispatch, getState) => {
    var token = await AsyncStorage.getItem('token')
    var intro = await AsyncStorage.getItem('intro')
    var patientId = await AsyncStorage.getItem('patientId')
    var city = await AsyncStorage.getItem('city')
    var name = await AsyncStorage.getItem('name')
    var userId = await AsyncStorage.getItem('userId')
    dispatch({
      type: 'CHECKLOGIN',
      payload: { token: token, intro: intro, patientId: patientId, city: city, name: name, userId: userId },
    });
  }
}

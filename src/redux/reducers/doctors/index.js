const initialState = {
  doctors: [],
  founddoctors: [],
  doccat: [],
};

export const doctors = (state = initialState, action) => {
  switch (action.type) {
    case 'FINDDOCTORS': {
      console.log(action.payload)
      return { ...state, doctors: action.payload };
    }
    case 'DOCCATLIST': {
      console.log(action.payload)
      return { ...state, doccat: action.payload };
    }
    default: {
      return state;
    }
  }
};
export default doctors;
const initialState = {
  login: {},
  logindata: {}
};

export const login = (state = initialState, action) => {
  switch (action.type) {
    case 'LOGIN': {
      return { ...state, login: action.payload };
    }
    case 'CHECKLOGIN': {
      return { ...state, logindata: action.payload };
    }
    default: {
      return state;
    }
  }
};
export default login;
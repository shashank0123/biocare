import React, { Component, useEffect } from 'react';
import {
  Text,
  View,
  Image,
  TouchableOpacity,
  FlatList,
  ScrollView,
  Dimensions,
  Animated,
} from 'react-native';
const { width, height } = Dimensions.get('window');
import styles from './style';
import { Icons, Images } from '../../utils';
import AsyncStorage from '@react-native-community/async-storage';
import LinearGradient from 'react-native-linear-gradient';
import { getLocation } from '../../redux/actions/getLocation';
import { getOffers } from '../../redux/actions/getOffers';
import { doctors } from '../../redux/actions/doctors';
import { getProfile } from '../../redux/actions/getProfile';
import { connect } from 'react-redux';
const iconItems = [
  {
    cat_id: '1',
    image: Images.DocHomeImg,
    title: 'Doctor',
    desc: 'Search doctor around you',
    screen: 'FindDoctors',
  },
  {
    cat_id: '2',
    image: Images.MedicineHomeImg,
    title: 'Pharmacy',
    desc: 'Order medicine home',
    screen: 'FindPharmacy',
  },
  {
    cat_id: '3',
    image: Images.DiagnosticHomeImg,
    title: 'Labs',
    desc: 'Book test at doorstep',
    screen: 'FindLabs',
  },
  {
    cat_id: '4',
    image: Images.VaccinationHomeImg,
    title: 'Vaccination',
    desc: 'View & Report Status online',
    screen: 'Vaccination',
  },
  {
    cat_id: '5',
    image: Images.InsuranceHomeImg,
    title: 'Insurance',
    desc: 'Book Health policy online',
    screen: 'Insurance',
  }];
const dots = [
  {
    b_id: '1',
  },
  {
    b_id: '2',
  },
];

const doctors1 = [
  {
    doc_id: '1',
    doc_img: Images.doc1Img,
    doc_name: 'Dr. Alina James',
    doc_desc: ' B.Sc, MBBS, DDVL, MD- Dermitologist',
    doc_rating: '4.2',
  },
  {
    doc_id: '2',
    doc_img: Images.doc2Img,
    doc_name: 'Dr. Steve Robert',
    doc_desc: ' B.Sc, MBBS, DDVL',
    doc_rating: '3.6',
  },
  {
    doc_id: '3',
    doc_img: Images.doc1Img,
    doc_name: 'Dr. Alina James',
    doc_desc: ' B.Sc, MBBS, DDVL, MD- Dermitologist',
    doc_rating: '4.2',
  },
  {
    doc_id: '4',
    doc_img: Images.doc2Img,
    doc_name: 'Dr. Steve Robert',
    doc_desc: ' B.Sc, MBBS, DDVL',
    doc_rating: '3.6',
  },
];
class Home extends Component {
  state = { token: "", offers: [], usersname: '', location: '', patientId: '', userId: '' }
  componentDidMount(props) {
    this._getStorageValue();
  }
  async _getStorageValue() {
    var value = await AsyncStorage.getItem('token')
    var patientId = await AsyncStorage.getItem('patientId')
    var city = await AsyncStorage.getItem('city')
    var name = await AsyncStorage.getItem('name')
    var userId = await AsyncStorage.getItem('userId')
    if (value != null) {
      console.log(patientId, 'Here is patient id.......')
      console.log(userId, 'Here is user id.......')
      this.setState({ token: value })
      this.setState({ patientId: patientId })
      this.setState({ city: city })
      this.setState({ name: name })
      this.setState({ userId: userId })
      if (!patientId) {
        this.props.navigation.navigate('Members')
      }

      // value2 = this.props.doctors(value, city, 1, '')

      this.getOffers(value);
      this.getProfile(value, patientId);
    }
    else
      return value
  }
  getProfile(token, patientId) {
    this.props.getProfile(token, patientId)
      .then(() => {
        console.log(this.props.getProfile1, 'ye h Profile response......')
        this.setState({ getProfile: this.props.getProfile1 })

      })


  }
  getOffers(token) {
    this.props.getOffers(token)
      .then(() => {
        console.log(this.props.getOffers1, ' ye h offer response......')
        this.setState({ getOffers: this.props.getOffers1 })
      })
  }
  scrollX = new Animated.Value(0); // this will be the scroll location of our ScrollView
  logout = async () => {
    await AsyncStorage.removeItem('token')
    await AsyncStorage.removeItem('patientId')
    await AsyncStorage.removeItem('name')
    await AsyncStorage.removeItem('city')
    this.props.navigation.navigate('Login')
  }
  offerRender() {
    if (this.props.getOffers1 != null) {
      return (
        <View style={{ flex: 1 }}>
          {/* <FlashMessage position="top" /> */}
          <View style={styles.BannerWrapper}>
            <FlatList
              horizontal={true}
              showsHorizontalScrollIndicator={false}
              data={this.state.getOffers}
              // data={banners}
              renderItem={({ item }) => (
                <View style={styles.bannerImgWrapper}>
                  <Image source={{ uri: item.imageUrl }} style={styles.bannerImg} />

                  {/* <Text> {item.name}</Text> */}
                </View>
              )}
              keyExtractor={item => item.id}
            />
          </View>
          <View style={styles.docTextBar}>
            <TouchableOpacity
              style={styles.docItemWrapper}
              activeOpacity={1}
            // onPress={() =>
            //   this.props.navigation.navigate('FindDoctors', {doctor_id : item.id})}
            >
              <Text style={[styles.docText, { color: '#5588E7' }]}>
                Doctors nearby you
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.docItemWrapper}
              activeOpacity={1}
              onPress={() =>
                // this.props.navigation.navigate('FindDoctors', { doctor_id: item.id })}
                this.props.navigation.navigate('FindDoctors')}
            >
              <Text style={[styles.docText, { color: 'rgb(85,136,231)', marginLeft: '30%' }]}>
                See All
              </Text>
            </TouchableOpacity>
          </View>
          <View style={styles.docListWrapper}>
            <FlatList
              horizontal={true}
              showsHorizontalScrollIndicator={false}
              // data={this.props.doctors1}
              data={doctors1}
              renderItem={({ item }) => (
                <TouchableOpacity
                  style={styles.docItemWrapper}
                  activeOpacity={1}
                  onPress={() =>
                    // this.props.navigation.navigate('DoctorScreen', { doctor_id: item.id })}
                    this.props.navigation.navigate('DoctorScreen')}
                >
                  <View style={styles.docItemUpper}>
                    <Image
                      // source={Images.doc1Img}
                      source={item.doc_img}
                      style={{ width: '100%', height: '100%', borderRadius: 50 }}
                    />
                  </View>
                  <View style={styles.docItemLower}>
                    <View style={styles.docItemLowerShadowWrap}>
                      <View style={styles.docItemLowerWrap}>
                        <Text style={[styles.docName, { marginTop: 0 }]}>
                          {/* {item.name} */}
                          {item.doc_name}
                        </Text>
                        <View style={styles.docDescWrapper}>
                          {/* <Text style={styles.docDesc}>{item.bio}</Text> */}
                          <Text style={styles.docDesc}>{item.doc_desc}</Text>
                        </View>
                        <View style={styles.ratingWrapper}>
                          <Image source={Images.starImg} style={{ height: 14.76, width: 15.52 }} />
                          <Text
                            style={[
                              styles.docText,
                              { marginLeft: 8, color: '#898A8F' },
                            ]}>
                            {/* {item.verified} */}
                            {item.doc_rating}
                          </Text>
                        </View>
                      </View>
                    </View>
                  </View>
                </TouchableOpacity>
              )}
              keyExtractor={item => item.id}
            />
          </View>
        </View>
      )
    }
    else {
      return (
        <View style={{ marginTop: height * 0.5 }}>
          <View style={styles.docTextBar}>
            <TouchableOpacity
              style={styles.docItemWrapper}
              activeOpacity={1}
            // onPress={() =>
            //   this.props.navigation.navigate('FindDoctors', {doctor_id : item.id})}
            >
              <Text style={[styles.docText, { color: '#5588E7' }]}>
                Doctors nearby you
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.docItemWrapper}
              activeOpacity={1}
              onPress={() =>
                // this.props.navigation.navigate('FindDoctors', { doctor_id: item.id })}
                this.props.navigation.navigate('FindDoctors')}
            >
              <Text style={[styles.docText, { color: 'rgb(85,136,231)', marginLeft: '30%' }]}>
                See All
              </Text>
            </TouchableOpacity>
          </View>
          <View style={styles.docListWrapper}>
            <FlatList
              horizontal={true}
              showsHorizontalScrollIndicator={false}
              // data={this.props.doctors1}
              data={doctors1}
              renderItem={({ item }) => (
                <TouchableOpacity
                  style={styles.docItemWrapper}
                  activeOpacity={1}
                  onPress={() =>
                    // this.props.navigation.navigate('DoctorScreen', { doctor_id: item.id })}
                    this.props.navigation.navigate('DoctorScreen')}
                >
                  <View style={styles.docItemUpper}>
                    <Image
                      // source={Images.doc1Img}
                      source={item.doc_img}
                      style={{ width: '100%', height: '100%', borderRadius: 50 }}
                    />
                  </View>
                  <View style={styles.docItemLower}>
                    <View style={styles.docItemLowerShadowWrap}>
                      <View style={styles.docItemLowerWrap}>
                        <Text style={[styles.docName, { marginTop: 0 }]}>
                          {/* {item.name} */}
                          {item.doc_name}
                        </Text>
                        <View style={styles.docDescWrapper}>
                          {/* <Text style={styles.docDesc}>{item.bio}</Text> */}
                          <Text style={styles.docDesc}>{item.doc_desc}</Text>
                        </View>
                        <View style={styles.ratingWrapper}>
                          <Image source={Images.starImg} style={{ height: 14.76, width: 15.52 }} />
                          <Text
                            style={[
                              styles.docText,
                              { marginLeft: 8, color: '#898A8F' },
                            ]}>
                            {/* {item.verified} */}
                            {item.doc_rating}
                          </Text>
                        </View>
                      </View>
                    </View>
                  </View>
                </TouchableOpacity>
              )}
              keyExtractor={item => item.id}
            />
          </View>
        </View>
      )
    }
  }
  render() {
    let position = Animated.divide(this.scrollX, width);
    return (
      <>
        <View style={styles.container}>
          <View style={styles.wrapper}>
            <LinearGradient
              colors={['#5588e7', '#75e4f7']}
              start={{ x: 0.16, y: 0.1 }}
              end={{ x: 1.1, y: 1.1 }}
              locations={[0.16, 50]}
              style={styles.upperCont}>
              <View style={styles.upperWrapper}>
                <TouchableOpacity
                  style={styles.profileImgWrapper}
                  activeOpacity={1}
                  onPress={() => this.props.navigation.navigate('Profile')}>
                  <Image
                    source={{ uri: this.props.getProfile1.avatarUrl }}
                    // source={Images.doc2Img}
                    style={{ width: '100%', height: '100%', borderRadius: 50 }}
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.userNameWrapper}
                  activeOpacity={1}
                  onPress={() => this.props.navigation.navigate('Profile')}>
                  <Text style={styles.uName}>Hi {this.props.getProfile1.name}</Text>



                </TouchableOpacity>
                <View style={styles.searchOptionWrapper}>
                  <TouchableOpacity
                    onPress={() => { this.props.navigation.navigate('Notifications') }}>
                    <Image
                      style={styles.topBarIcon}
                      source={Icons.NotifIcon} />
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => { this.props.navigation.navigate('NewCart') }}>
                    <Image
                      style={styles.topBarIcon}
                      source={Icons.CartIcon} />
                  </TouchableOpacity>
                </View>
              </View>
            </LinearGradient>
            <View style={styles.iconSliderWrapper}>
              <FlatList
                horizontal={true}
                showsHorizontalScrollIndicator={false}
                data={iconItems}
                pagingEnabled={true}
                the onScroll prop will pass a nativeEvent object to a function
                onScroll={Animated.event(
                  [{ nativeEvent: { contentOffset: { x: this.scrollX } } }], { useNativeDriver: false }
                )}
                scrollEventThrottle={16}
                renderItem={({ item }) => (
                  <View style={styles.iconItemsWrapper}>
                    <View style={styles.sliderItemWrapper}>
                      <TouchableOpacity
                        activeOpacity={1}
                        onPress={() =>
                          this.props.navigation.navigate(item.screen)
                        }
                        style={styles.itemImgWrapper}>
                        <Image source={item.image} style={styles.itemImg} />
                      </TouchableOpacity>
                      <Text style={styles.itemText}>{item.title}</Text>
                      <Text style={styles.subText}>{item.desc}</Text>
                    </View>
                  </View>
                )}
                keyExtractor={item => item.cat_id}
              />
              <View style={styles.scrollIndicatorWrapper}>
                {dots.map((_, i) => {
                  // the _ just means we won't use that parameter
                  let opacity = position.interpolate({
                    inputRange: [i - 1, i, i + 1], // each dot will need to have an opacity of 1 when position is equal to their index (i)
                    outputRange: [0.3, 1, 0.3], // when position is not i, the opacity of the dot will animate to 0.3
                    // inputRange: [i - 0.50000000001, i - 0.5, i, i + 0.5, i + 0.50000000001], // only when position is ever so slightly more than +/- 0.5 of a dot's index
                    // outputRange: [0.3, 1, 1, 1, 0.3], // is when the opacity changes from 1 to 0.3
                    extrapolate: 'clamp', // this will prevent the opacity of the dots from going outside of the outputRange (i.e. opacity will not be less than 0.3)
                  });
                  return (
                    <Animated.View // we will animate the opacity of the dots so use Animated.View instead of View here
                      key={i} // we will use i for the key because no two (or more) elements in an array will have the same index
                      style={{
                        opacity,
                        height: 8,
                        width: 8,
                        backgroundColor: '#5588E7',
                        margin: 8,
                        borderRadius: 5,
                      }}
                    />
                  );
                })}
                {/* <View style={styles.scrollIndicatorBig} />
                <View style={styles.scrollIndicatorSmall} />
                <View style={styles.scrollIndicatorSmall} /> */}
              </View>
            </View>
            {this.offerRender()}
          </View>
        </View>
      </>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    getLocation1: state.getLocation.getLocation,
    getOffers1: state.getOffers.getOffers,
    getProfile1: state.getProfile.getProfile,
    doctors1: state.doctors.doctors
  };
};
export default connect(mapStateToProps, {
  getLocation, getOffers, getProfile, doctors
})(Home);
import React, { Component } from 'react';
import {
  Text,
  View,
  Image,
  TouchableOpacity,
  ImageBackground,
  Dimensions,
} from 'react-native';
const { width, height } = Dimensions.get('window');
import styles from './style';
import { Icons, Images } from '../../utils';
//Libraries
import LinearGradient from 'react-native-linear-gradient';
import PhoneInput from 'react-native-phone-input';
import SwipeButton from 'rn-swipe-button';
import { pharmacy } from '../../redux/actions/pharmacy';
import { connect } from 'react-redux';
class SocialLogin extends Component {
  constructor() {
    super();
    this.state = {
      // btn1:Images.socialLoginArrow ,
      btn1:Icons.goIcon ,
      btn2:Icons.goIcon,
      btn3: Icons.goIcon,
    };
  }
  render() {
    return (
      <>
        <View style={styles.wrapper}>
          <View style={styles.bg}>
            <View style={styles.upperCont}>
              <TouchableOpacity
                style={styles.arrowCont}
                onPress={() => this.props.navigation.goBack()}>
                <Image source={Icons.BackIcon2} />
              </TouchableOpacity>
            </View>
            <View style={styles.textCont}>
              <Text style={styles.heading}>Choose an account</Text>
            </View>
            <View style={styles.socialCont}>
              <View style={styles.socialRow}>
                <View style={styles.iconTxtCont}>
                  <Image source={Images.fbLogo} style={styles.fbLogo} />
                  <Text style={styles.socialText}>Facebook</Text>
                </View>
                <View style={styles.btnCont}>
                  <SwipeButton
                    height={height * 0.03}
                    width={width * 0.2}                 
                       thumbIconImageSource={this.state.btn1}
                    onSwipeStart={() =>
                      this.setState({ btn1:Icons.goIconWhite })
                    }
                    onSwipeFail={() =>
                      this.setState({ btn1:Icons.goIcon })
                    }
                    onSwipeSuccess={() => alert('Facebook Login pressed')}
                    containerStyles={styles.btn}
                    railBackgroundColor={'#ffffff'}
                    railFillBackgroundColor={'#59A3EE'}
                    railFillBorderColor={'#59A3EE'}
                    thumbIconBackgroundColor={'#ffffff'}
                    thumbIconStyles={{
                      borderWidth: 0,
                      width:10,
                      height:10
                    }}
                    title=""
                  />
                </View>
              </View>
              <View style={styles.socialRow}>
                <View style={styles.iconTxtCont}>
                  <Image source={Images.googleLogo} style={styles.googleLogo} />
                  <Text style={styles.socialText}>Google</Text>
                </View>
                <View style={styles.btnCont}>
                  <SwipeButton
                     height={height * 0.03}
                     width={width * 0.2}  
                    thumbIconImageSource={this.state.btn2}
                    onSwipeStart={() =>
                      this.setState({ btn2:Icons.goIconWhite })
                    }
                    onSwipeFail={() =>
                      this.setState({ btn2:Icons.goIcon})
                    }
                    onSwipeSuccess={() => alert('Google login pressed')}
                    containerStyles={styles.btn}
                    railBackgroundColor={'#ffffff'}
                    railFillBackgroundColor={'#59A3EE'}
                    railFillBorderColor={'#59A3EE'}
                    thumbIconBackgroundColor={'#ffffff'}
                    thumbIconStyles={{
                      borderWidth: 0,
                     
                    }}
                    title=""
                  />
                </View>
              </View>
              <View style={styles.socialRow}>
                <View style={styles.iconTxtCont}>
                  <Image source={Images.appleLogo} style={styles.appleLogo} />
                  <Text style={styles.socialText}>Apple</Text>
                </View>
                <View style={styles.btnCont}>
                  <SwipeButton
                     height={height * 0.03}
                     width={width * 0.2}  
                    thumbIconImageSource={this.state.btn3}
                    onSwipeStart={() =>
                      this.setState({ btn3:Icons.goIconWhite})
                    }
                    onSwipeFail={() =>
                      this.setState({ btn3: Icons.goIcon})
                    }
                    onSwipeSuccess={() => alert('Apple login pressed')}
                    containerStyles={styles.btn}
                    railBackgroundColor={'#ffffff'}
                    railFillBackgroundColor={'#59A3EE'}
                    railFillBorderColor={'#59A3EE'}
                    thumbIconBackgroundColor={'#ffffff'}
                    thumbIconStyles={{
                      borderWidth: 0,
                    }}
                    title=""
                  />
                </View>
              </View>
            </View>
          </View>
        </View>
      </>
    );
  }
}
const mapStateToProps = (state) => {
  console.log(state.pharmacy.pharmacy)
  return {
    pharmacy1: state.pharmacy.pharmacy
  };
};
export default connect(mapStateToProps, {
  pharmacy
})(SocialLogin);

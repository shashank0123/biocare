import React, { Component } from 'react';
import { Text, View, Image, TouchableOpacity, ScrollView, Alert } from 'react-native';
import styles from './style';
import { Icons, Images } from '../../utils';
import { TextInput, FlatList, BorderlessButton } from 'react-native-gesture-handler';
import { ListItem, SearchBar } from 'react-native-elements';
//Libraries
import LinearGradient from 'react-native-linear-gradient';
import { color } from 'react-native-reanimated';
const doctors = [
  {
    cat_id: '1',
    cat_img: Images.starImg,
    cat_name: 'Change Password',
    screenName: 'ChangePassword',
  },
  {
    cat_id: '2',
    cat_img: Images.starImg,
    cat_name: 'Fill Profile',
    screenName: 'FillProfile',
  },
  {
    cat_id: '3',
    cat_img: Images.starImg,
    cat_name: 'About Us',
    screenName: 'AboutUs',
  },
  {
    cat_id: '4',
    cat_img: Images.starImg,
    cat_name: 'FAQ',
    screenName: 'AboutUs',
  },
  // {
  //   cat_id: '5',
  //   cat_img: Images.starImg,
  //   cat_name: 'ClinicalScreen',
  //   screenName: 'ClinicalScreen',
  // },
  // {
  //   cat_id: '6',
  //   cat_img: Images.starImg,
  //   cat_name: 'Doctor Catagories',
  //   screenName: 'DoctorCatagories',
  // },
  // {
  //   cat_id: '7',
  //   cat_img: Images.starImg,
  //   cat_name: 'Doc Filteration',
  //   screenName: 'DocFilteration',
  // },
  // {
  //   cat_id: '8',
  //   cat_img: Images.starImg,
  //   cat_name: 'FillProfile',
  //   screenName: 'FillProfile',
  // },
  // {
  //   cat_id: '9',
  //   cat_img: Images.starImg,
  //   cat_name: 'FindAndBook',
  //   screenName: 'FindAndBook',
  // },
  // {
  //   cat_id: '10',
  //   cat_img: Images.starImg,
  //   cat_name: 'LabTest',
  //   screenName: 'LabTest',
  // },
  // {
  //   cat_id: '10',
  //   cat_img: Images.starImg,
  //   cat_name: 'MedicalRecords',
  //   screenName: 'MedicalRecords',
  // },
  // {
  //   cat_id: '10',
  //   cat_img: Images.starImg,
  //   cat_name: 'MyOrdersFilter',
  //   screenName: 'MyOrdersFilter',
  // },
  // {
  //   cat_id: '11',
  //   cat_img: Images.starImg,
  //   cat_name: 'Prescription_Picker',
  //   screenName: 'Prescription_Picker',
  // },

];
class Settings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      data: [],
      error: null,
    };
    this.arrayholder = [];
  }
  render() {
    return (
      <>
        <View style={styles.container}>
          <View style={styles.wrapper}>
            <LinearGradient
              colors={['#5588e7', '#75e4f7']}
              start={{ x: 0.16, y: 0.1 }}
              end={{ x: 1.1, y: 1.1 }}
              locations={[0.16, 50]}
              style={styles.upperCont}>
              <View style={styles.upperWrapper}>
                <TouchableOpacity
                  style={styles.backIconWrapper}
                  activeOpacity={1}
                  onPress={() => this.props.navigation.goBack()}>
                  <Image source={Icons.BackIcon} />
                </TouchableOpacity>
                <View style={styles.imageWrapper}>
                  <Text style={styles.uName}>Settings</Text>
                </View>
                <View style={styles.settingIconWrapper} />
              </View>
            </LinearGradient>
          </View>
          <View style={styles.docListWrapper}>
            <FlatList
              scrollEnabled={true}
              showsVerticalScrollIndicator={false}
              data={doctors}
              renderItem={({ item }) => (
                <View style={styles.docTypeWrapper}>
                  <View style={styles.textBox}>
                    <Text style={styles.specialText}>{item.cat_name}</Text>
                  </View>
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate(item.screenName)
                    }>
                    <Image source={Icons.goIcon} style={styles.iconStyle} />
                  </TouchableOpacity>
                </View>
              )}
              keyExtractor={item => item.cat_id}
            />
          </View>
        </View>
      </>
    );
  }
}
export default Settings;
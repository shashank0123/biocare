import React, { Component } from 'react';
import { Text, View, Image, TouchableOpacity, ScrollView, Alert, Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window');
import styles from './style';
import { Icons, Images } from '../../utils';
//Libraries
import LinearGradient from 'react-native-linear-gradient';
;
import { add_family_member } from '../../redux/actions/add_family_member';
import { connect } from 'react-redux';
import { TextInput } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-community/async-storage';
import DatePicker from 'react-native-datepicker'

import ImagePicker from 'react-native-image-crop-picker';
import RadioGroup from 'react-native-radio-buttons-group';
import { Picker } from '@react-native-community/picker';
class AddMember extends Component {
  state = {
    token: '', name: "", email: "", phone: "", dob: '', city: '', blood_group: 'Blood Group', gender: '', profile_pic: Images.plus, image: '',
    radio_props: [
      { label: 'Male', value: '0', color: '#5588e7', },
      { label: 'Female', value: '1', color: '#5588e7', },
      { label: 'Other', value: '2', color: '#5588e7', },
    ],
  }
  componentDidMount() {
    this.selectRadio()
    // this._getStorageValue()
  }
  submitForm() {
    // this.props.add_family_member(this.state)
    this.props.navigation.navigate('Profile')
    // redirect anywhere you want
  }
  // async _getStorageValue() {
  //   var value = await AsyncStorage.getItem('token')
  //   if (value != null) {
  //     this.setState({ token: value })
  //     // console.log(value2)
  //     // if (value2.data == undefined){
  //     //   await AsyncStorage.removeItem('token')
  //     //   this.props.navigation.navigate('Login')
  //     // }
  //     // this.props.navigation.navigate('AppHome')
  //   }
  //   else
  //     // this.props.navigation.navigate('Login')
  //     return value
  // }
  // chooseFile = () => {
  //   console.log("start")
  //   var options = {
  //     title: 'Select Image',
  //     customButtons: [
  //       { name: 'customOptionKey', title: 'Choose Photo from Custom Option' },
  //     ],
  //     storageOptions: {
  //       skipBackup: true,
  //       path: 'images',
  //     },
  //   };
  //   ImagePicker.showImagePicker(options, (response) => {
  //     console.log('Response = ', response);
  //     if (response.didCancel) {
  //       console.log('User cancelled image picker');
  //     } else if (response.error) {
  //       console.log('ImagePicker Error: ', response.error);
  //     } else if (response.customButton) {
  //       console.log('User tapped custom button: ', response.customButton);
  //       alert(response.customButton);
  //     } else {
  //       let source = response;
  //       // You can also display the image using data:
  //       //let source = { uri: 'data:image/jpeg;base64,' + response.data };
  //       this.setState({
  //         profile_pic: source,
  //       });
  //     }
  //   });
  // };
  onPress = radio_props => {
    this.setState({ radio_props })
    this.selectRadio()
  };
  selectRadio() {
    let selectedButton = this.state.radio_props.find(e => e.selected == true);
    selectedButton = selectedButton ? selectedButton.value : this.state.data[0].value;
    this.setState({ gender: selectedButton })
  }
  selectProfileImage = () => {
    ImagePicker.openPicker({
      width: 300,
      height: 400,
      cropping: true
    }).then(image => {
      console.log('Image is here', image);
      this.setState({ profile_image: image.path })
    });
  }
  render() {
    let selectedButton = this.state.radio_props.find(e => e.selected == true);
    selectedButton = selectedButton ? selectedButton.value : this.state.radio_props[0].label;
    return (
      <View style={styles.container}>
        <View style={styles.wrapper}>
          <LinearGradient
            colors={['#5588e7', '#75e4f7']}
            start={{ x: 0.16, y: 0.1 }}
            end={{ x: 1.1, y: 1.1 }}
            locations={[0.16, 50]}
            style={styles.upperCont}>
            <View style={styles.upperWrapper}>
              <TouchableOpacity
                style={styles.backIconWrapper}
                activeOpacity={1}
                onPress={() => this.props.navigation.goBack()}>
                <Image source={Icons.BackIcon} />
              </TouchableOpacity>
              <View style={styles.titleWrapper}>
                <Text style={styles.uName}>Add Family Member</Text>
              </View>
            </View>
          </LinearGradient>
          <View style={styles.boxCont1}>
            <ScrollView>
              <View style={styles.plusImageWrapper}>
                <TouchableOpacity
                  activeOpacity={1}
                  // onPress={() => { this.chooseFile() }}
                  onPress={this.selectProfileImage} >
                  <Image source={this.state.profile_image ? { uri: this.state.profile_image } : Images.plus} style={styles.plus} />
                </TouchableOpacity>
              </View>
              <View style={styles.innerBoxWrapper}>
                <TextInput
                  style={styles.innerBox}
                  // value={this.state.name}
                  // onChangeText={name => this.setState({ name })}
                  placeholder="Name"
                  placeholderTextColor='#000' />
              </View>
              <View style={styles.innerBoxWrapper}>
                <TextInput
                  style={styles.innerBox}
                  // value={this.state.email}
                  // onChangeText={email => this.setState({ email })}
                  placeholder="E-mail" 
                  placeholderTextColor='#000'/>
              </View>
              <View style={styles.innerBoxWrapper}>
                <TextInput
                  style={styles.innerBox}
                  // value={this.state.phone}
                  // onChangeText={phone => this.setState({ phone })}
                  placeholder="Mobile Number"
                  placeholderTextColor='#000' />
              </View>
              
              <View style={styles.innerBoxWrapper}>
                <Picker
                  selectedValue={this.state.blood_group}
                  style={styles.innerBox1}
                  onValueChange={(itemValue, itemIndex) =>
                    this.setState({ blood_group: itemValue })
                  }>
                  <Picker.Item label="A+" value="A+" />
                  <Picker.Item label="A-" value="A-" />
                  <Picker.Item label="B+" value="B+" />
                  <Picker.Item label="B-" value="B-" />
                  <Picker.Item label="AB+" value="AB+" />
                  <Picker.Item label="AB-" value="AB-" />
                  <Picker.Item label="O+" value="O+" />
                  <Picker.Item label="O-" value="O-" />
                </Picker>
              </View>
              <View style={styles.genderInnerBoxWrapper}>
                <RadioGroup radioButtons={this.state.radio_props} onPress={(this.onPress)} flexDirection='row' />
              </View>
              <View style={styles.cityInnerBoxWrapper}>
                <TextInput
                  style={styles.innerBox}
                  // value={this.state.city}
                  // onChangeText={city => this.setState({ city })}
                  placeholder="City" 
                  placeholderTextColor='#000'/>
              </View>
            </ScrollView>
          </View>
          <View style={styles.bfbottomWrapper}>
            <TouchableOpacity
              activeOpacity={1}
              onPress={() => { this.props.navigation.navigate('Members') }}>
              <Text style={styles.feedbackTextbtn}>Cancel</Text>
            </TouchableOpacity>
            <LinearGradient
              colors={['#5588e7', '#75e4f7']}
              start={{ x: 0.1, y: 0.1 }}
              end={{ x: 0.3, y: 1.9 }}
              locations={[0.1, 0.6]}
              style={styles.bookTextbtnWrapper}>
              <TouchableOpacity
                activeOpacity={1}
                style={styles.bookBtn}
                onPress={() => this.submitForm()}>
                <Text style={[styles.bookTextbtn, { color: '#ffffff' }]}>
                  Add
                </Text>
              </TouchableOpacity>
            </LinearGradient>
          </View>
        </View>
      </View>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    add_family_member: state.add_family_member,
  };
};
export default connect(mapStateToProps, {
  add_family_member
})(AddMember);

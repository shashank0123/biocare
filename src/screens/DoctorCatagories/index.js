import React, { Component } from 'react';
import { Text, View, Image, TouchableOpacity, ScrollView } from 'react-native';
import styles from './style';
import { Icons, Images } from '../../utils';
import { TextInput, FlatList } from 'react-native-gesture-handler';
import axios from 'axios';
//Libraries
import LinearGradient from 'react-native-linear-gradient';
import { pharmacy } from '../../redux/actions/pharmacy';
import AsyncStorage from '@react-native-community/async-storage';
import { connect } from 'react-redux';
import { finddoctors } from '../../redux/actions/doctors';
class DoctorCatagories extends Component {
  state = { doctors: [] };
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      data: [],
      error: null,
      Test: [],
      arrayholder: [],
    };
  }
  componentDidMount() {
    this._getStorageValue();
    if (this.props.route.params.city) {
      this.setState({
        doctor_category: this.props.route.params.doc_category.value,
        category_name: this.props.route.params.doc_category.label,
        city: this.props.route.params.city,
        appointment_type: this.props.route.params.appointment_type,
      });
    }


  }
  async _getStorageValue() {
    var value = await AsyncStorage.getItem('token');
    var patientId = await AsyncStorage.getItem('patientId');
    var name = await AsyncStorage.getItem('name');
    var userId = await AsyncStorage.getItem('userId');
    if (value != null) {
      this.setState({
        token: value,
        patientId: patientId,
        name: name,
        userId: userId,
      });
      // console.log(this.state, 'shashahnk');
      this.props.finddoctors(this.state, value).then(() => {
        console.log(this.props.doctors1, 'newdata')
        this.setState({ data: this.props.doctors1 });
      });
      if (!patientId) {
        this.props.navigation.navigate('Members');
      }
    } else {
      return value;
    }
  }
  searchFilterFunction = text => {
    this.setState({
      value: text,
    });
    const newData = this.state.arrayholder.filter(item => {
      const itemData = `${item.name.toUpperCase()}`;
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });
    this.setState({
      data: newData,
    });
  };
  render(props) {
    return (
      <>
        <View style={styles.container}>
          <View style={styles.wrapper}>
            <LinearGradient
              colors={['#5588e7', '#75e4f7']}
              start={{ x: 0.16, y: 0.1 }}
              end={{ x: 1.1, y: 1.1 }}
              locations={[0.16, 50]}
              style={styles.upperCont}>
              <View style={styles.upperWrapper}>
                <TouchableOpacity
                  style={styles.backIconWrapper}
                  activeOpacity={1}
                  onPress={() => this.props.navigation.goBack()}>
                  <Image source={Icons.BackIcon} />
                </TouchableOpacity>
                <View style={styles.imageWrapper}>
                  <Text style={styles.uName}>Doctor Categories</Text>
                </View>
                <View style={styles.settingIconWrapper} />
              </View>
            </LinearGradient>
            <View style={styles.inputWrapper}>
              <Image source={Icons.searchIcon} />
              <TextInput
                style={styles.searchinp}
                onChangeText={text => this.searchFilterFunction(text)}
                placeholder="Doctors, specialities, clinics"
                placeholderTextColor="#000"
              />
            </View>
          </View>
          <View style={styles.topText}>
            <Text style={styles.texthead}>Top Specialities</Text>
          </View>
          <View style={styles.docListWrapper}>
            <FlatList
              scrollEnabled={true}
              showsVerticalScrollIndicator={false}
              data={this.state.data}
              renderItem={({ item }) => (
                <View style={styles.docTypeWrapper}>
                  <Image style={styles.doccatImg} source={{ uri: 'https://biocare-backend.s3.ap-south-1.amazonaws.com/kyc/Ed52uwj8pB5Oc54YPQ0EIbNO9Wt9ny4yPgfr4n3t.png' }} />
                  <View style={styles.textBox}>
                    <Text style={styles.specialText}>{item.name}</Text>
                  </View>
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate('DocDetailedCategory', {
                        doctor_category: item.id,
                        category_name: item.name,
                        city: this.props.route.params.city,
                        appointment_type:
                          this.props.route.params.appointment_type,
                      })
                    }>
                    <Image source={Icons.goIcon} style={styles.iconStyle} />
                  </TouchableOpacity>
                </View>
              )}
              keyExtractor={item => item.id}
            />
          </View>
        </View>
      </>
    );
  }
}
const mapStateToProps = state => {
  console.log(state.doctors, 'statedata');
  return {
    doctors1: state.doctors.doctors,
  };
};
export default connect(mapStateToProps, {
  finddoctors,
})(DoctorCatagories);

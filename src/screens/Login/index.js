import React, { Component } from 'react';
import {
  Text,
  View,
  Image,
  TouchableOpacity,
  ImageBackground,
  Dimensions,
  KeyboardAvoidingView,
  ScrollView,
  BackHandler, TextInput, Alert
} from 'react-native';
const { width, height } = Dimensions.get('window');
import styles from './style';
import { Icons, Images } from '../../utils';
//Libraries
import AppIntroSlider from 'react-native-app-intro-slider';
import PhoneInput from 'react-native-phone-input';
import SwipeButton from 'rn-swipe-button';
import { pharmacy } from '../../redux/actions/pharmacy';
import { login, checklogin } from '../../redux/actions/login';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
const introSlider = [
  {
    text: 'Delivery at your doorstep.',
    image: Images.medicineLogo,
    bg: '#59b2ab',
  },
  {
    text:
      'Too busy to see a doctor? Now, book an appointment or consult a doctor at ease.',
    image: Images.doctorsLogo,
    bg: '#febe29',
  },
  {
    text:
      'Free sample collection from home, now makes it easy and convenient for patients to check their lab test results online.',
    image: Images.labtestLogo,
    bg: '#22bcb5',
  },
  {
    text:
      'Buy an affordable medical insurance policy online for family, individuals and senior citizens.',
    image: Images.insuranceLogo,
    bg: '#22bcb5',
  },
];
class Login extends Component {
  constructor() {
    super();
    this.state = {
      showRealApp: true,
      showHomePage: false,
      showError: false,
      valid: '',
      errorMsg: 'Enter Valid Credentials',
      type: '',
      value: '',
      btn: Images.loginBtnArrow,
      phone: '',
      password: ''
    };
    // this.updateInfo = this.updateInfo.bind(this);
  }
  static navigationOptions = navigation => ({
    header: null
  });

  componentDidMount() {
    this._getStorageValue();
  }

  async _getStorageValue() {
    this.props.checklogin().then(() => {
      console.log(this.props.checklogin1)
      var intro = this.props.checklogin1.intro
      if (this.props.checklogin1.token != ''){
        this.props.navigation.navigate('AppHome')
      }
      if (intro == null) {
        this.setState({ showRealApp: false })
      }
    })
  }
  //added
  async submitHandler() {
    this.setState({ loadingApp: true })
    this.props.login(this.state).then(async () => {
      this.props.checklogin().then(() => {
        this.setState({ loadingApp: false })
        if (this.props.checklogin1.token == null) {
          Alert.alert(
            'Biocare',
            // this.props.login1.error
            'Please Check Your Credentials'
          )
        }
        else {
          this.props.navigation.navigate('AppHome')
        }
      })
    })

  }
  _renderItem = ({ item }) => {
    return (
      <View style={styles.slideCont}>
        <View style={styles.imgCont}>
          <Image
            source={item.image}
            style={styles.img}
            resizeMode={'contain'}
          />
        </View>
        <View style={styles.textCont2}>
          <Text style={styles.text}>{item.text}</Text>
        </View>
      </View>
    );
  };

  _onDone = () => {
    this.setState({ showRealApp: true });
    this.markIntro();

  };
  _onSkip = () => {
    this.setState({ showRealApp: true });
    this.markIntro();
  };

  async markIntro() {
    await AsyncStorage.setItem('intro', 'checked');
  }


  _renderNextButton = () => {
    return (
      <View>
        <Image source={Images.bannerBtn} style={{ marginRight: width * 0.02 }} />
      </View>
    );
  };
  _renderSkipButton = () => {
    return (
      <View style={styles.skipBox}>
        <Text style={styles.skipText}>Skip</Text>
      </View>
    );
  };
  _renderDoneButton = () => {
    return (
      <View>
        <Image source={Images.bannerBtn} style={{ marginRight: width * 0.02 }} />
      </View>
    );
  };
  renderErrorMsg() {
    if (this.state.showError) {
      return (<Text style={styles.loginError}>{this.state.errorMsg}</Text>);
    }
  }

  render() {
    if (this.state.showRealApp) {
      return (
        <>
          <View style={styles.wrapper}>
            <ImageBackground source={Images.loginBg} style={styles.bgImage}>
              <KeyboardAvoidingView
                keyboardVerticalOffset={30} // adjust the value here if you need more padding
                behavior="padding">
                <ScrollView
                  contentContainerStyle={styles.scrollWrapper}
                  showsVerticalScrollIndicator={false}>
                  <View style={styles.logoCont}>
                    <Image
                      source={Images.loginLogo}
                      style={styles.loginLogo}
                    />
                  </View>
                  <View style={styles.textCont}>
                    <Text style={styles.heading}>Get healthy with Biocare</Text>

                  </View>

                  <TextInput style={styles.textInputStyle}
                    placeholder="Enter Mobile Number"
                    placeholderTextColor='#000000'

                    underlineColorAndroid="transparent"
                    value={this.state.phone}
                    onChangeText={phone => this.setState({ phone })}
                    textAlignVertical='center'
                  />
                  <TextInput style={styles.textInputStyle2}
                    placeholder="Password"
                    placeholderTextColor='#000000'
                    underlineColorAndroid="transparent"
                    secureTextEntry={true}
                    value={this.state.password}
                    onChangeText={password => this.setState({ password })}
                    textAlignVertical='center' />
                  <View style={styles.btnCont}>
                    <SwipeButton
                      thumbIconImageSource={this.state.btn}
                      onSwipeStart={() =>
                        this.setState({ btn: Images.loginBtnArrow2 })
                      }
                      onSwipeFail={() =>
                        this.setState({ btn: Images.loginBtnArrow })
                      }
                      onSwipeSuccess={() => {
                        this.submitHandler()
                      }
                      }
                      railBackgroundColor={'#ffffff'}
                      railBorderColor={'#59A3EE'}
                      railFillBackgroundColor={'#59A3EE'}
                      railFillBorderColor={'#59A3EE'}
                      thumbIconBackgroundColor={'#ffffff'}
                      thumbIconStyles={{ borderWidth: 0 }}
                      title=""
                    />
                  </View>

                  <TouchableOpacity
                    style={{ marginTop: 20 }}
                    onPress={() =>
                      this.props.navigation.navigate('SocialLogin')
                    }>
                    <Text style={styles.socialText}>
                      Or connect using social account
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate('Registration')
                    }>
                    <Text style={styles.dontHaveAccountText}>
                      <Text style={{ color: 'blue' }}> Forgot Password</Text>
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate('Registration')
                    }>
                    <Text style={styles.dontHaveAccountText}>
                      <Text style={{ color: 'blue' }}> Signup</Text>
                    </Text>
                  </TouchableOpacity>
                  {/* </View> */}
                </ScrollView>
              </KeyboardAvoidingView>
            </ImageBackground>
          </View>
        </>
      );
    } else {
      return (
        <AppIntroSlider
          renderItem={this._renderItem}
          data={introSlider}
          onDone={this._onDone}
          onSkip={this._onSkip}
          dotStyle={{ backgroundColor: '#CBD5D7' }}
          activeDotStyle={{ backgroundColor: '#5588E7' }}
          renderSkipButton={this._renderSkipButton}
          renderNextButton={this._renderNextButton}
          renderDoneButton={this._renderDoneButton}
          showSkipButton={true}
        />
      );
    }
  }
}
const mapStateToProps = (state) => {
  return {
    login1: state.login.login,
    checklogin1: state.login.logindata
  };
};
export default connect(mapStateToProps, {
  login, checklogin
})(Login);

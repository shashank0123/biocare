import {StyleSheet, Dimensions} from 'react-native';
const {width, height} = Dimensions.get('window');
export default StyleSheet.create({
  container: {
    width,
    height,
    position: 'relative',
    backgroundColor: 'pink',
  },
  wrapper: {
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
    position: 'absolute',
    left: '0%',
    right: '0%',
    top: '0%',
    bottom: '0%',
    backgroundColor: '#F5F5F5',
  },
  upperCont: {
    position: 'absolute',
    left: '0%',
    right: '0%',
    top: '-3.4%',
    bottom: '78.54%',
    borderBottomLeftRadius: 40,
    borderBottomRightRadius: 40,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  upperWrapper: {
    width: '100%',
    height: height * 0.22,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  backIconWrapper: {
    width: width * 0.2,
    height: height * 0.05,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  titleWrapper: {
    width: width * 0.4,
    height: width * 0.15,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  settingIconWrapper: {
    width: width * 0.22,
    height: height * 0.032,
    borderRadius: 50,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    marginLeft: width * 0.09,
    backgroundColor: '#ffffff',
  },
  uName: {
    fontFamily: 'Helvetica Neue',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: width * 0.04,
    lineHeight: 25,
    color: '#fff',
  },
  dropName: {
    fontFamily: 'Helvetica Neue',
    fontStyle: 'normal',
    fontWeight: '500',
    fontSize: width * 0.022,
    color: 'rgb(85,136,231)',
  },
//  .......................//
  
   
  
  
   
    loginArea: {
        paddingHorizontal: 10,
        width:'90%',
        paddingVertical: 10,
        height: height * 0.25,
        borderWidth: 1,
        marginHorizontal: 20,
        borderRadius: 25,
        marginTop:'5%',
        borderColor: 'white',
        elevation: 3,
        backgroundColor: "#ffffff"
    },
    input1: {
        width: '95%',
        paddingLeft:10,
        color:'#000'
      
    },
    formBox: {
        // justifyContent: 'center',
        alignItems: 'center',
        width: "90%",
        // height:'31%',
        flexDirection: 'row',
        borderRadius: 30,
        // borderWidth:1,
        marginTop: 15,
        backgroundColor: "#fff",
        elevation: 2,
        paddingLeft:10
    },
    formBox1: {
        justifyContent: 'center',
        // alignItems: 'center',
        width: "90%",
       
        borderRadius: 30,
       
        marginTop: 15,
        backgroundColor: "#fff",
        elevation: 2,
        padding:15
       
    },
    boxBtn2: {
        width: width * 0.8,
        height: height * 0.07,
       
        backgroundColor: '#6271f7',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 30,
        // marginRight: width * 0.018,
        marginTop: width * 0.12
        
    },
    btnText: {
        fontWeight: 'normal',
        textShadowColor: '#ffffff',
        fontSize: 20,
        color: '#FFFFFF'
    },
}
);

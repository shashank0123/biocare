import React, { Component } from 'react';
import { Text, View, Image, TouchableOpacity, ScrollView, Alert, Button, Dimensions, TextInput } from 'react-native';
import styles from './style';
import { Icons, Images } from '../../utils';
const { width, height } = Dimensions.get('window');
//Libraries
import LinearGradient from 'react-native-linear-gradient';
import StarRating from 'react-native-star-rating';
class NewFeedback extends Component {
    state = {
        rating: 0,
        name: '',
        email: '',
        feedback: ''
    }
    onStarRatingPress(rating) {
        this.setState({
            rating: rating
        });
    }
    render() {
        return (
            <>
                <View style={styles.container}>
                    <View style={styles.wrapper}>
                        <LinearGradient
                            colors={['#5588e7', '#75e4f7']}
                            start={{ x: 0.16, y: 0.1 }}
                            end={{ x: 1.1, y: 1.1 }}
                            locations={[0.16, 50]}
                            style={styles.upperCont}>
                            <View style={styles.upperWrapper}>
                                <TouchableOpacity
                                    style={styles.backIconWrapper}
                                    activeOpacity={1}
                                    onPress={() => this.props.navigation.goBack()}>
                                    <Image source={Icons.BackIcon} />
                                </TouchableOpacity>
                                <View style={styles.titleWrapper}>
                                    <Text style={styles.uName}>New Feedback</Text>
                                </View>
                                <TouchableOpacity
                                    activeOpacity={1}
                                    style={styles.settingIconWrapper}
                                    onPress={() => {
                                        this.props.navigation.navigate("Members", { screen: 'Videos' })
                                    }}>
                                    {/* <Text style={styles.dropName}>{this.state.name}</Text> */}
                                    <Text style={styles.dropName}>Satyam</Text>
                                </TouchableOpacity>
                            </View>
                        </LinearGradient>
                        <View style={{ justifyContent: 'center', alignItems: 'center', width: '100%', marginTop: '45%' }}>
                            <View style={styles.formBox}>
                                <TextInput placeholderTextColor="#000" 
                                    style={styles.input1} 
                                    placeholder="Full Name"
                                    value={this.state.name}
                                    onChangeText={name => this.setState({ name })}
                                />
                            </View>
                            <View style={styles.formBox}>
                                <TextInput placeholderTextColor="#000" 
                                    style={styles.input1} placeholder="Email"
                                    value={this.state.email}
                                    onChangeText={email => this.setState({ email })}
                                />
                            </View>
                            <View style={styles.loginArea}>
                                <Text style={{ paddingLeft: 10, fontWeight: 'bold' }}>Please Enter Feedback: </Text>
                                <TextInput placeholderTextColor="#000" autoCapitalize='none' multiline={true} textAlignVertical='top'
                                    style={styles.input1} placeholder=""
                                    value={this.state.feedback}
                                    onChangeText={feedback => this.setState({ feedback })}
                                />
                            </View>
                            <View style={styles.formBox1}>
                                <Text style={{ paddingLeft: 10, fontWeight: 'bold' }}>Please Give Rating: </Text>
                                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                    <StarRating fullStarColor="#ffc121"
                                        emptyStarColor="grey"
                                        starSize={33}
                                        disabled={false}
                                        maxStars={5}
                                        rating={this.state.rating}
                                        selectedStar={(rating) => this.onStarRatingPress(rating)}
                                        containerStyle={{ width: 250, marginTop: 10 }} />
                                </View>
                            </View>
                            <LinearGradient
                                colors={['#5588e7', '#75e4f7']}
                                start={{ x: 0.1, y: 0.1 }}
                                end={{ x: 0.3, y: 1.9 }}
                                locations={[0.1, 0.6]}
                                style={styles.boxBtn2}>
                                <TouchableOpacity
                                    activeOpacity={1}
                                // style={styles.oxBtn2}
                                >
                                    <Text style={[styles.btnText, { color: '#ffffff' }]}>
                                        Submit
                                    </Text>
                                </TouchableOpacity>
                            </LinearGradient>
                        </View>
                    </View>
                </View>
            </>
        );
    }
}
export default NewFeedback;

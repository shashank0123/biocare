import React, { Component } from 'react';
import { Text, View, Image, TouchableOpacity, ScrollView, Alert, Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window');
import styles from './style';
import { Icons, Images } from '../../utils';
//Libraries
import LinearGradient from 'react-native-linear-gradient';
;
import { saveprofile } from '../../redux/actions/update_profile';
import { getProfile } from '../../redux/actions/getProfile';
import { connect } from 'react-redux';
import { TextInput } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-community/async-storage';
import DatePicker from 'react-native-datepicker'
import ImagePicker from 'react-native-image-crop-picker';
import RadioGroup from 'react-native-radio-buttons-group';
import { Picker } from '@react-native-community/picker';
class FillProfile extends Component {
  state = {
    token: '', patientId: '', userId: '', profile_image: '', name: "", email: "", password: '', phone: "", bloodGroup: '', gender: '', dob: '', city: '', pincode: '', adress: '', country: '', state: '', image: '',
    radio_props: [
      { label: 'Male', value: '0', color: '#5588e7', },
      { label: 'Female', value: '1', color: '#5588e7', },
      { label: 'Other', value: '2', color: '#5588e7', },
    ],
  }
  componentDidMount() {
    this.selectRadio()
    this._getStorageValue()
  }
  async _getStorageValue() {
    var value = await AsyncStorage.getItem('token')
    var patientId = await AsyncStorage.getItem('patientId')
    var userId = await AsyncStorage.getItem('userId')
    if (value != null) {
      console.log(patientId, 'Here is patient id.......')
      console.log(userId, 'Here is user id.......')
      this.setState({ token: value })
      this.setState({ patientId: patientId })
      this.setState({ userId: userId })
      if (!patientId) {
        this.props.navigation.navigate('Members')
      }
      this.getProfile(value, patientId);
    }
    else
      return value
  }
  getProfile(token, patientId) {
    this.props.getProfile(token, patientId)
      .then(() => {
        console.log(this.props.getProfile1, 'ye h Profile response......')
        this.setState({ getProfile: this.props.getProfile1 })
        this.setState({
          profile_image: this.props.getProfile1.avatarUrl,
          name: this.props.getProfile1.name,
          city: this.props.getProfile1.city,
          phone: this.props.getProfile1.phone,
          dob: this.props.getProfile1.dob,
          dob: this.props.getProfile1.dob,
          city: this.props.getProfile1.city,
          bloodGroup: this.props.getProfile1.bloodGroup,
          gender: this.props.getProfile1.gender,
          email: this.props.getProfile1.email,
          adress: this.props.getProfile1.adress,
          state: this.props.getProfile1.state,
          pincode: this.props.getProfile1.pincode,
          country: this.props.getProfile1.country,
        })
      })
  }
  submitForm() {
    // console.log(this.state)
    this.props.saveprofile(this.state.token, this.state).then(() => {
      console.log(this.props.saveprofile1, 'check this........')
      // this.props.navigation.replace('Login')
    })
  }
  onPress = radio_props => {
    this.setState({ radio_props })
    this.selectRadio()
  };
  selectRadio() {
    let selectedButton = this.state.radio_props.find(e => e.selected == true);
    selectedButton = selectedButton ? selectedButton.value : this.state.data[0].value;
    this.setState({ gender: selectedButton })
  }
  selectProfileImage = () => {
    ImagePicker.openPicker({
      width: 300,
      height: 400,
      cropping: true
    }).then(image => {
      console.log('Image is here', image);
      this.setState({ profile_image: image.path })
    });
  }
  render() {
    let selectedButton = this.state.radio_props.find(e => e.selected == true);
    selectedButton = selectedButton ? selectedButton.value : this.state.radio_props[0].label;
    return (
      <View style={styles.container}>
        <View style={styles.wrapper}>
          <LinearGradient
            colors={['#5588e7', '#75e4f7']}
            start={{ x: 0.16, y: 0.1 }}
            end={{ x: 1.1, y: 1.1 }}
            locations={[0.16, 50]}
            style={styles.upperCont}>
            <View style={styles.upperWrapper}>
              <TouchableOpacity
                style={styles.backIconWrapper}
                activeOpacity={1}
                onPress={() => this.props.navigation.goBack()}>
                <Image source={Icons.BackIcon} />
              </TouchableOpacity>
              <View style={styles.titleWrapper}>
                <Text style={styles.uName}>Complete Profile</Text>
              </View>
            </View>
          </LinearGradient>
          <View style={styles.boxCont1}>
            <ScrollView>
              <View style={styles.plusImageWrapper}>
                <TouchableOpacity
                  activeOpacity={1}
                  // onPress={() => { this.chooseFile() }}
                  onPress={this.selectProfileImage} >
                  <Image source={this.state.profile_image ? { uri: this.state.profile_image } : Images.plus} style={styles.plus} />
                </TouchableOpacity>
              </View>
              <View style={styles.innerBoxWrapper}>
                <TextInput
                  style={styles.innerBox}
                  value={this.state.name}
                  onChangeText={name => this.setState({ name })}
                  placeholder="Name"
                  placeholderTextColor='#000' />
              </View>
              <View style={styles.innerBoxWrapper}>
                <TextInput
                  style={styles.innerBox}
                  value={this.state.email}
                  onChangeText={email => this.setState({ email })}
                  placeholder="E-mail"
                  placeholderTextColor='#000' />
              </View>
              <View style={styles.innerBoxWrapper}>
                <TextInput
                  style={styles.innerBox}
                  value={this.state.password}
                  onChangeText={password => this.setState({ password })}
                  placeholder="Password"
                  placeholderTextColor='#000' />
              </View>
              <View style={styles.innerBoxWrapper}>
                <TextInput
                  style={styles.innerBox}
                  value={this.state.phone}
                  onChangeText={phone => this.setState({ phone })}
                  placeholder="Mobile Number"
                  placeholderTextColor='#000' />
              </View>
              <View style={styles.innerBoxWrapper}>
                <Picker
                  selectedValue={this.state.bloodGroup}
                  style={styles.innerBox1}
                  onValueChange={(itemValue, itemIndex) =>
                    this.setState({ bloodGroup: itemValue })
                  }>
                  <Picker.Item label="A+" value="A+" />
                  <Picker.Item label="A-" value="A-" />
                  <Picker.Item label="B+" value="B+" />
                  <Picker.Item label="B-" value="B-" />
                  <Picker.Item label="AB+" value="AB+" />
                  <Picker.Item label="AB-" value="AB-" />
                  <Picker.Item label="O+" value="O+" />
                  <Picker.Item label="O-" value="O-" />
                </Picker>
              </View>
              <View style={styles.genderInnerBoxWrapper}>
                <RadioGroup radioButtons={this.state.radio_props} onPress={(this.onPress)} flexDirection='row' />
              </View>
              <View style={styles.cityInnerBoxWrapper}>
                <TextInput
                  style={styles.innerBox}
                  value={this.state.dob}
                  onChangeText={dob => this.setState({ dob })}
                  placeholder="DOB"
                  placeholderTextColor='#000' />
              </View>
              <View style={styles.cityInnerBoxWrapper}>
                <TextInput
                  style={styles.innerBox}
                  value={this.state.city}
                  onChangeText={city => this.setState({ city })}
                  placeholder="City"
                  placeholderTextColor='#000' />
              </View>
              <View style={styles.cityInnerBoxWrapper}>
                <TextInput
                  style={styles.innerBox}
                  value={this.state.pincode}
                  onChangeText={pincode => this.setState({ pincode })}
                  placeholder="Pincode"
                  placeholderTextColor='#000' />
              </View>
              <View style={styles.cityInnerBoxWrapper}>
                <TextInput
                  style={styles.innerBox}
                  value={this.state.adress}
                  onChangeText={adress => this.setState({ adress })}
                  placeholder="Adress"
                  placeholderTextColor='#000' />
              </View>
              <View style={styles.cityInnerBoxWrapper}>
                <TextInput
                  style={styles.innerBox}
                  value={this.state.country}
                  onChangeText={country => this.setState({ country })}
                  placeholder="Country"
                  placeholderTextColor='#000' />
              </View>
              <View style={styles.cityInnerBoxWrapper}>
                <TextInput
                  style={styles.innerBox}
                  value={this.state.state}
                  onChangeText={state => this.setState({ state })}
                  placeholder="State"
                  placeholderTextColor='#000' />
              </View>
            </ScrollView>
          </View>
          <View style={styles.bfbottomWrapper}>
            <TouchableOpacity
              activeOpacity={1}
              onPress={() => { this.props.navigation.navigate('Members') }}>
              <Text style={styles.feedbackTextbtn}>Cancel</Text>
            </TouchableOpacity>
            <LinearGradient
              colors={['#5588e7', '#75e4f7']}
              start={{ x: 0.1, y: 0.1 }}
              end={{ x: 0.3, y: 1.9 }}
              locations={[0.1, 0.6]}
              style={styles.bookTextbtnWrapper}>
              <TouchableOpacity
                activeOpacity={1}
                style={styles.bookBtn}
                onPress={() => this.submitForm()}>
                <Text style={[styles.bookTextbtn, { color: '#ffffff' }]}>
                  Submit
                </Text>
              </TouchableOpacity>
            </LinearGradient>
          </View>
        </View>
      </View>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    saveprofile1: state.saveprofile.saveprofile,
    getProfile1: state.getProfile.getProfile,
  };
};
export default connect(mapStateToProps, {
  saveprofile, getProfile
})(FillProfile);

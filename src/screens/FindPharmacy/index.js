import React, { Component } from 'react';
import { Text, View, Image, TouchableOpacity, ScrollView, Alert, Modal, FlatList, Button } from 'react-native';
import RadioGroup from 'react-native-radio-buttons-group';
import styles from './style';
import { Icons, Images } from '../../utils';
//Libraries
import LinearGradient from 'react-native-linear-gradient';
;
import { TextInput } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-community/async-storage';
import { checklogin } from '../../redux/actions/login';
import { connect } from 'react-redux';
const data = [
  {
    id: '1',
    name: 'pharmacy001',
  },
  {
    id: '2',
    name: 'pharmacy002',
  },
];
class FindPharmacy extends Component {
  async _getStorageValue() {
   this.props.checklogin().then(() => {
     console.log(this.props.checklogin1)
     this.setState({ name : this.props.checklogin1.name, city : this.props.checklogin1.city})})
  }
  state = {
    showModal: false,
    categoryText: 'pharmacy001',
    radio_props: [
      { label: 'Pick-Up', value: '0', color: '#5588e7', },
      { label: 'Home Delivery', value: '1', color: '#5588e7', }
    ],
    Lab_Cat: [],
    value1: 0,
    value2: 0,
    city: '',
    name: '',
    delivery_type: '',
  }
  onPress = radio_props => {
    this.setState({ radio_props })
    this.selectRadio()
  };
  selectRadio() {
    let selectedButton = this.state.radio_props.find(e => e.selected == true);
    selectedButton = selectedButton ? selectedButton.value : this.state.data[0].value;
    this.setState({ delivery_type: selectedButton })
  }
  componentDidMount() {
    this.selectRadio()
    this._getStorageValue()
  }
  render() {
    let selectedButton = this.state.radio_props.find(e => e.selected == true);
    selectedButton = selectedButton ? selectedButton.value : this.state.radio_props[0].label;
    console.log(this.state.delivery_type)
    return (
      <View style={styles.container}>
        <View style={styles.wrapper}>
          <LinearGradient
            colors={['#5588e7', '#75e4f7']}
            start={{ x: 0.16, y: 0.1 }}
            end={{ x: 1.1, y: 1.1 }}
            locations={[0.16, 50]}
            style={styles.upperCont}>
            <View style={styles.upperWrapper}>
              <TouchableOpacity
                style={styles.backIconWrapper}
                activeOpacity={1}
                onPress={() => this.props.navigation.goBack()}>
                <Image source={Icons.BackIcon} />
              </TouchableOpacity>
              <View style={styles.titleWrapper}>
                <Text style={styles.uName}>Find Pharmacies</Text>
              </View>
              <TouchableOpacity
                activeOpacity={1}
                style={styles.settingIconWrapper}
                onPress={() => {
                  this.props.navigation.navigate("Members", { screen: 'FindPharmacy' })
                }}>
                {/* <Text style={styles.dropName}>{this.state.name}</Text> */}
                <Text style={styles.dropName}>{this.state.name}</Text>
              </TouchableOpacity>
            </View>
          </LinearGradient>
          <View style={styles.boxCont1}>
            <Text style={styles.inputHeads}>City</Text>
            <View style={styles.innerBox}>
              <Image style={styles.doclocImg} source={Icons.locationIcon} />
              <TextInput
                style={styles.textInputBox}
                value={this.state.city}
                onChangeText={city => this.setState({ city })}
                placeholder="City" 
                placeholderTextColor='#000'/>
            </View>
            <Text style={styles.inputHeads}>Delivery Type</Text>
            <Text></Text>
            <View>
              <RadioGroup radioButtons={this.state.radio_props} onPress={(this.onPress)} flexDirection='row' />
            </View>
            <LinearGradient
              colors={['#5588e7', '#75e4f7']}
              start={{ x: 0.1, y: 0.1 }}
              end={{ x: 0.3, y: 1.9 }}
              locations={[0.1, 0.6]}
              style={styles.bookTextbtnWrapper}>
              <TouchableOpacity
                activeOpacity={1}
                style={styles.bookBtn}
                // onPress={() => this.props.navigation.navigate('Pharmacy', { city: this.state.city, delivery_type: this.state.delivery_type })}
                onPress={() => this.props.navigation.navigate('Pharmacy')}
              >
                <Text style={[styles.bookTextbtn, { color: '#ffffff' }]}>
                  Find Pharmacies
                </Text>
              </TouchableOpacity>
            </LinearGradient>
          </View>
        </View>
      </View>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    checklogin1: state.login.logindata
  };
};
export default connect(mapStateToProps, {
  checklogin
})(FindPharmacy);
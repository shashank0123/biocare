import React, { Component } from 'react';
import { Text, View, Image, TouchableOpacity, ScrollView, Alert } from 'react-native';
import styles from './style';
import { Icons, Images } from '../../utils';
//Libraries
import LinearGradient from 'react-native-linear-gradient';
import { FlatList } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-community/async-storage';
import AwesomeAlert from 'react-native-awesome-alerts';
import { my_appointments } from '../../redux/actions/my_appointments';
import { cancelAppointment } from '../../redux/actions/cancelAppointment';
import { connect } from 'react-redux';
import { getProfile } from '../../redux/actions/getProfile';
const docCatTypes = [
  {
    doc_cat_id: '1',
    doc_cat_name: 'Unconfirmed',
  },
  {
    doc_cat_id: '2',
    doc_cat_name: 'Confirmed',
  },
  {
    doc_cat_id: '3',
    doc_cat_name: 'Online Booking',
  },
  {
    doc_cat_id: '4',
    doc_cat_name: 'In Person',
  },
];
const doctors = [
  {
    doc_id: '1',
    doc_img: Images.doc1Img,
    doc_name: 'Dr. Alina James',
    doc_desc: ' B.Sc, MBBS, DDVL, MD- Dermitologist',
    doc_rating: '4.2',
  },
  {
    doc_id: '2',
    doc_img: Images.doc2Img,
    doc_name: 'Dr. Steve Robert',
    doc_desc: ' B.Sc, MBBS, DDVL',
    doc_rating: '3.6',
  },
  {
    doc_id: '3',
    doc_img: Images.doc1Img,
    doc_name: 'Dr. Alina James',
    doc_desc: ' B.Sc, MBBS, DDVL, MD- Dermitologist',
    doc_rating: '4.2',
  },
  {
    doc_id: '4',
    doc_img: Images.doc2Img,
    doc_name: 'Dr. Steve Robert',
    doc_desc: ' B.Sc, MBBS, DDVL',
    doc_rating: '3.6',
  },
];
class Appointments extends Component {
  state = { Appoint: [], name: '', showCancelAlert: false, Alert_doctor_name: '', filter_id: '', token: '', patientId: '' }
  componentDidMount(props) {
    this._getStorageValue();
  }
  addfilter(item) {
    this.setState({ filter_id: item.doc_cat_id })
    // var filter = '&filter='+item.doc_cat_name
    // this.props.my_appointments(this.state.token, this.state.patient_id, filter)
  }
  async _getStorageValue() {
    var value = await AsyncStorage.getItem('token')
    var patientId = await AsyncStorage.getItem('patientId')
    var city = await AsyncStorage.getItem('city')
    var name = await AsyncStorage.getItem('name')
    if (value != null) {
      console.log(patientId, 'Here is patient id.......')
      this.setState({ token: value })
      this.setState({ patientId: patientId })
      this.setState({ city: city })
      this.setState({ name: name })
      if (!patientId) {
        this.props.navigation.navigate('Members')
      }
      this.getProfile(value, patientId);
      this.getMyAppointments(value, patientId);
    }
    else
      return value
  }
  getProfile(token, patientId) {
    this.props.getProfile(token, patientId)
      .then(() => {
        console.log(this.props.getProfile1, 'ye h Profile response......')
        this.setState({ getProfile: this.props.getProfile1 })
      })
  }
  getMyAppointments(token, patientId) {
    this.props.my_appointments(token, patientId)
      .then(() => {
        // console.log(this.props.my_appointments1, 'ye h Appointments response......')
        this.setState({ my_appointments: this.props.my_appointments1 })
      })
  }
  // categoryDefined(category){
  //   if (JSON.stringify(category, typeof value === 'undefined' ? null : value)!='{}'){
  //       return(
  //         <Text style={styles.docprofText}>
  //         {(JSON.stringify(category, typeof value === 'undefined' ? null : value))}
  //         </Text>
  //     )
  //   }
  // }
  // showCancelAlert = () => {
  //   this.setState({
  //     showCancelAlert: true
  //   });
  // };
  // hideBookingAlert = () => {
  //   this.setState({
  //     showCancelAlert: false
  //   });
  // };
  renderButton(item) {
    if (item.status == 0) {
      return (<><View style={styles.moneyWrapper}>
        <Image source={Icons.moneyIcon} />
        <Text style={styles.moneyText}>
          Unpaid
        </Text>
      </View><TouchableOpacity
        activeOpacity={1}
        style={styles.boxBtn2}
        onPress={() => {
          // this.setState({ Alert_doctor_name: item.doctor_name })
          // this.cancelAppointment(item.id)
          // this.props.my_appointments(this.state.token, this.state.patient_id)
        }
        }>
          <LinearGradient
            colors={['#5588e7', '#75e4f7']}
            start={{ x: 0.1, y: 0.1 }}
            end={{ x: 0.3, y: 1.9 }}
            locations={[0.1, 0.6]}
            style={styles.boxBtn2}>
            <Text
              style={[styles.btnText, { color: '#ffffff' }]}>
              Cancel
            </Text>
          </LinearGradient>
        </TouchableOpacity></>);
    }
    if (item.status == 1) {
      return (<><View style={styles.moneyWrapper}>
        <Image source={Icons.moneyIcon} />
        <Text style={styles.moneyText}>
          Unpaid
        </Text>
      </View><TouchableOpacity
        activeOpacity={1}
        style={styles.boxBtn2}
        onPress={() =>
          this.payAppointment()
        }>
          <LinearGradient
            colors={['#5588e7', '#75e4f7']}
            start={{ x: 0.1, y: 0.1 }}
            end={{ x: 0.3, y: 1.9 }}
            locations={[0.1, 0.6]}
            style={styles.boxBtn2}>
            <Text
              style={[styles.btnText, { color: '#ffffff' }]}>
              Pay
            </Text>
          </LinearGradient>
        </TouchableOpacity></>);
    }
    if (item.status == 2) {
      return (<><View style={styles.moneyWrapper}>
        <Image source={Icons.moneyIcon} />
        <Text style={styles.moneyText}>
          Paid
        </Text>
      </View><TouchableOpacity
        activeOpacity={1}
        style={styles.boxBtn2}
        onPress={() =>
          this.props.navigation.navigate("Invoice")
        }>
          <LinearGradient
            colors={['#5588e7', '#75e4f7']}
            start={{ x: 0.1, y: 0.1 }}
            end={{ x: 0.3, y: 1.9 }}
            locations={[0.1, 0.6]}
            style={styles.boxBtn2}>
            <Text
              style={[styles.btnText, { color: '#ffffff' }]}>
              Invoice
            </Text>
          </LinearGradient>
        </TouchableOpacity></>);
    }
    if (item.status == -1) {
      return (<><View style={styles.moneyWrapper}>
        <Image source={Icons.moneyIcon} />
        <Text style={styles.moneyText}>
          Cancelled
        </Text>
      </View></>);
    }
  }
  // cancelAppointment(item_id){
  //   this.showCancelAlert()
  //   this.props.cancelAppointment(this.state.token, this.state.patient_id, item_id)
  //   this.props.my_appointments(this.state.token, this.state.patient_id)
  // }
  render() {
    return (
      <>
        <View style={styles.container}>
          <View style={styles.wrapper}>
            <LinearGradient
              colors={['#5588e7', '#75e4f7']}
              start={{ x: 0.16, y: 0.1 }}
              end={{ x: 1.1, y: 1.1 }}
              locations={[0.16, 50]}
              style={styles.upperCont}>
              <View style={styles.upperWrapper}>
                <TouchableOpacity
                  style={styles.backIconWrapper}
                  activeOpacity={1}
                  onPress={() => this.props.navigation.goBack()}>
                  <Image source={Icons.BackIcon} />
                </TouchableOpacity>
                <View style={styles.titleWrapper}>
                  <Text style={styles.uName}>Appointments</Text>
                </View>
                <TouchableOpacity
                  activeOpacity={1}
                  style={styles.settingIconWrapper}
                  onPress={() => {
                    this.props.navigation.navigate("Members", { screen: 'Appointments' })
                  }}>
                  <Text style={styles.dropName}>{this.state.name}</Text>
                </TouchableOpacity>
              </View>
              <View style={styles.catTypeWrapper}>
                <FlatList
                  horizontal={true}
                  showsHorizontalScrollIndicator={false}
                  data={docCatTypes}
                  renderItem={({ item }) => (
                    <TouchableOpacity
                      activeOpacity={1}
                      onPress={() => {
                        this.addfilter(item);
                      }}>
                      <Text style={[styles.catText, { backgroundColor: this.state.filter_id == item.doc_cat_id ? '#5588e7' : 'white' }]}>{item.doc_cat_name}</Text>
                    </TouchableOpacity>
                  )}
                  keyExtractor={item => item.doc_cat_id}
                />
              </View>
            </LinearGradient>
            <View style={styles.docContWrapper}>
              <ScrollView>
                <View style={styles.docCont2}>
                  <FlatList
                    showsVerticalScrollIndicator={false}
                    data={this.state.my_appointments}
                    renderItem={({ item }) => (
                      <>
                        <View style={styles.docDetailedWrapper2}>
                          <View
                            style={[styles.docSpecsWrapper, { marginTop: 20 }]}>
                            <Image
                              style={{ width: 50, height: 50, borderRadius: 50 }}
                              source={{ uri: item.doctorProfilePicUrl }}
                            />
                            {/* <Text style={styles.perText}>{ }</Text> */}
                            <View style={styles.docNameWrapper}>
                              <TouchableOpacity
                                activeOpacity={1}
                                onPress={() => {
                                  // this.props.navigation.navigate('DoctorScreen', {doctor_id: item.doctor_id})
                                  this.props.navigation.navigate('DoctorScreen')
                                }}>
                                <Text style={styles.docNameText}>
                                  {item.doctorName}
                                </Text>
                                <Text style={styles.docSubNameText}>
                                  {item.specialization}
                                </Text>
                                {/* {this.categoryDefined(item.category)} */}
                                {/* <Text style={styles.docExpText}>
                                  {item.exp} years of experience
                               
                              </Text> */}
                              </TouchableOpacity>
                            </View>
                          </View>
                          <View style={styles.middleWrapper}>
                            {/* <Text style={styles.feedText}>9 Feedback</Text> */}
                            <Text style={styles.dateText}>{item.date}</Text>
                            <Text style={styles.timeText}>{item.time}</Text>
                          </View>
                          <View style={styles.bottom1Wrapper}>
                            <View style={styles.nodoctorWrapper}>
                              <Image
                                style={styles.doclocImg}
                                source={Icons.locationIcon}
                              />
                              <Text style={styles.nodocText}>{item.locationName}</Text>
                            </View>
                          </View>
                          <View style={styles.bottom2Wrapper}>
                            {this.renderButton(item)}
                          </View>
                        </View>
                      </>
                    )}
                    keyExtractor={item => item.doc_id}
                  />
                </View>
              </ScrollView>
            </View>
            {/* <AwesomeAlert
              show={this.state.showCancelAlert}
              showProgress={false}
              title={"Cancel Appointment"}
              message={"Your appointment with " + (this.state.Alert_doctor_name)+ " has been cancelled"}
              closeOnTouchOutside={true}
              closeOnHardwareBackPress={false}
              //showCancelButton={true}
              showConfirmButton={true}
              confirmText="Okay"
              confirmButtonColor="#5588e7"
              onConfirmPressed={() => {
                this.hideBookingAlert();
              }}
            /> */}
          </View>
        </View>
        <View style={styles.filterBtnWrapper}>
        </View>
      </>
    );
  }
}
const mapStateToProps = (state) => {
  console.log(state.my_appointments.my_appointments)
  return {
    my_appointments1: state.my_appointments.my_appointments,
    getProfile1: state.getProfile.getProfile,
    cancelAppointment1: state.cancelAppointment.cancelAppointment,
  };
};
export default connect(mapStateToProps, {
  my_appointments, getProfile, cancelAppointment
})(Appointments);

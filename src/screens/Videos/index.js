import React, { Component } from 'react';
import { Text, View, Image, TouchableOpacity, ScrollView, Alert, Button } from 'react-native';
import styles from './style';
import { Icons, Images } from '../../utils';
//Libraries
import LinearGradient from 'react-native-linear-gradient';
import { FlatList } from 'react-native-gesture-handler';
import { getLocation } from '../../redux/actions/getLocation';
import { videoList } from '../../redux/actions/videoList';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { getProfile } from '../../redux/actions/getProfile';
import VideoPlayer from 'react-native-video-player';

class Videos extends Component {
  state = {
    location: '',
    name: '',
    token: '',
    patient_id: '',
    videos: [],
  }

  videoPlayer;

  async _getStorageValue() {
    var value = await AsyncStorage.getItem('token');
    var patientId = await AsyncStorage.getItem('patientId');
    var name = await AsyncStorage.getItem('name');
    var userId = await AsyncStorage.getItem('userId');
    if (value != null) {
      this.setState({
        token: value,
        patientId: patientId,
        name: name,
        userId: userId,
      });
      // console.log(this.state, 'shashahnk');
      this.props.videoList(patientId, value).then(() => {
        console.log(this.props.videoList1, 'newdata')
        this.setState({ videos: this.props.videoList1 });
      });
      if (!patientId) {
        this.props.navigation.navigate('Members');
      }
    } else {
      return value;
    }
  }

  componentDidMount() {
    this._getStorageValue();

  }


  render() {
    return (
      <>
        <View style={styles.container}>
          <View style={styles.wrapper}>
            <LinearGradient
              colors={['#5588e7', '#75e4f7']}
              start={{ x: 0.16, y: 0.1 }}
              end={{ x: 1.1, y: 1.1 }}
              locations={[0.16, 50]}
              style={styles.upperCont}>
              <View style={styles.upperWrapper}>
                <TouchableOpacity
                  style={styles.backIconWrapper}
                  activeOpacity={1}
                  onPress={() => this.props.navigation.goBack()}>
                  <Image source={Icons.BackIcon} />
                </TouchableOpacity>
                <View style={styles.titleWrapper}>
                  <Text style={styles.uName}>Videos</Text>
                </View>
                <TouchableOpacity
                  activeOpacity={1}
                  style={styles.settingIconWrapper}
                  onPress={() => {
                    this.props.navigation.navigate("Members", { screen: 'Videos' })
                  }}>
                  {/* <Text style={styles.dropName}>{this.state.name}</Text> */}
                  <Text style={styles.dropName}>Satyam</Text>
                </TouchableOpacity>
              </View>
            </LinearGradient>
            <View style={styles.docContWrapper}>
              {/* <ScrollView> */}
              {/* <View style={styles.docCont2}> */}
              <FlatList
                showsVerticalScrollIndicator={false}
                contentContainerStyle={styles.docCont2}
                // data={this.props.videoList1}
                data={this.state.videos}
                renderItem={({ item }) => (
                  <View style={styles.videoCard}>
                    <View style={styles.videoWrapper}>
                      <VideoPlayer
                        // endWithThumbnail
                        video={{ uri: item.videoUrl }}
                        // videoWidth={'100%'}
                        // videoHeight={'30%'}
                        ref={r => this.player = r}
                        style={styles.videoImg}
                      />
                      {/* <Image source={Images.video} style={styles.videoImg} /> */}
                    </View>
                    <View style={styles.textWrapper}>
                      <View style={styles.FieldView}>
                        <Text style={styles.header}>Video: </Text>
                        {/* <Text style={styles.info}>{item.video_title}</Text> */}
                        <Text style={styles.info}>{item.title}</Text>
                      </View>
                      <View style={styles.FieldView}>
                        <Text style={styles.header}>Doctor: </Text>
                        {/* <Text style={styles.info}>{item.video_title}</Text> */}
                        <Text style={styles.info}>{item.doctorName}</Text>
                      </View>
                      <View style={styles.FieldView}>
                        <Text style={styles.header}>Date: </Text>
                        {/* <Text style={styles.info}>{item.created_at.split('T')[0]}</Text> */}
                        <Text style={styles.info}>2-2-2021</Text>
                      </View>
                    </View>
                  </View>
                )}
                keyExtractor={item => item.id}
              />
            </View>
            {/* </ScrollView> */}
          </View>
          {/* </View> */}
        </View>
      </>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    videoList1: state.videoList.videoList,
    getProfile1: state.getProfile.getProfile,
  };
};
export default connect(mapStateToProps, {
  videoList, getProfile
})(Videos);

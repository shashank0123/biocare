import React, { Component } from "react";
import {
    View,
    Text,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    TextInput,
    Dimensions,
    Image, Alert
} from "react-native";
const { width, height } = Dimensions.get('window');
import styles from './style';
import { Icons, Images } from '../../utils';
import LinearGradient from 'react-native-linear-gradient';
import { registeration } from '../../redux/actions/registeration';
import { connect } from 'react-redux';
class Registration extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            city: '',
            password: '',
            phone: '',
        }
        // this._getStorageValue();
    }
    // async _getStorageValue() {
    //     var value = await AsyncStorage.getItem('token')
    //     if (value != null) {
    //         console.log(value)
    //         this.props.navigation.navigate('AppHome')
    //     }
    //     return value
    // }
    submitNext() {
        var error = 0;
        var message = 'something went wrong';
        if (this.state.name.length < 3) {
            error = 1;
            message = 'Enter proper name'
        }
        if (this.state.phone.length < 9) {
            error = 1;
            message = 'Enter valid phone number'
        }
        if (error != 1) {
            this.props.registeration(this.state).then(async () => {
                
                console.log(this.props.registeration1)
                this.props.navigation.navigate('Login')
            })
        }
        else {
            Alert.alert(
                'Biocare',
                message,
            )
        }
    }
    render() {
        return (
            <View style={styles.wrapper}>
                <KeyboardAvoidingView behavior="padding">
                    <ScrollView
                        contentContainerStyle={styles.scrollWrapper}
                        showsVerticalScrollIndicator={false}>
                        <View style={styles.logoCont}>
                            <Image
                                source={Images.loginLogo}
                                style={{ width: width * 0.35, height: height * 0.20 }}
                            />
                        </View>
                        <View style={styles.textCont}>
                            <Text style={styles.heading}>Enter Your details to register</Text>
                            {/* <Text style={}>link</Text> */}
                        </View>
                        <TextInput style={styles.textInputStyle}
                            placeholder="Name"
                            placeholderTextColor='#000000'
                            underlineColorAndroid="transparent"
                            textAlignVertical='center'
                            value={this.state.name}
                            onChangeText={name => this.setState({ name })}
                        />
                       
                        <TextInput style={styles.textInputStyle2}
                            placeholder="Phone Number"
                            placeholderTextColor='#000000'
                            underlineColorAndroid="transparent"
                            textAlignVertical='center'
                            keyboardType={"number-pad"}
                            maxLength={10}
                            value={this.state.phone}
                            onChangeText={phone => this.setState({ phone })}
                        />
                        <TextInput style={styles.textInputStyle2}
                            placeholder="Password"
                            secureTextEntry={true}
                            placeholderTextColor='#000000'
                            underlineColorAndroid="transparent"
                            textAlignVertical='center'
                            value={this.state.password}
                            onChangeText={password => this.setState({ password })}
                        />
                         <TextInput style={styles.textInputStyle2}
                            placeholder="City"
                            placeholderTextColor='#000000'
                            underlineColorAndroid="transparent"
                            textAlignVertical='center'
                            value={this.state.city}
                            onChangeText={city => this.setState({ city })}
                        />
                        <LinearGradient
                            colors={['#5588e7', '#75e4f7']}
                            start={{ x: 0.1, y: 0.1 }}
                            end={{ x: 0.3, y: 1.9 }}
                            locations={[0.1, 0.6]}
                            style={styles.bookTextbtnWrapper}>
                            <TouchableOpacity
                                activeOpacity={1}
                                style={styles.bookBtn}
                                onPress={() => this.submitNext()}>
                                <Text style={[styles.bookTextbtn, { color: '#ffffff' }]}>
                                    Register
                                </Text>
                            </TouchableOpacity>
                        </LinearGradient>
                    </ScrollView>
                </KeyboardAvoidingView>
            </View>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        registeration1: state.registeration.registeration
    };
};
export default connect(mapStateToProps, {
    registeration
})(Registration);